# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* sale_simple_requested_date
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 8.0rc1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-09-25 09:33+0000\n"
"PO-Revision-Date: 2014-09-25 09:33+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: sale_simple_requested_date
#: help:sale.order,requested_date:0
#: help:stock.picking,requested_date:0
msgid "Date by which the customer has requested the items to be delivered. Leave it blank if the customer didn't specify a date."
msgstr ""

#. module: sale_simple_requested_date
#: model:ir.model,name:sale_simple_requested_date.model_stock_picking
msgid "Picking List"
msgstr ""

#. module: sale_simple_requested_date
#: field:sale.order,requested_date:0
#: field:stock.picking,requested_date:0
msgid "Requested date"
msgstr ""

#. module: sale_simple_requested_date
#: model:ir.model,name:sale_simple_requested_date.model_sale_order
msgid "Sales Order"
msgstr ""

