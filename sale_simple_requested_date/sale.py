# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv, fields


class sale_order(osv.Model):

    _inherit = 'sale.order'
    
    _columns = {
        #'requested_date': fields.date('Fecha entrega', help="Fecha en la que el cliente solicita que se le entregue la mercancía. Déjelo en blanco si no procede."),
        'requested_date': fields.date('Requested date', help="Date by which the customer has requested the items to be delivered. Leave it blank if the customer didn't specify a date."),
    }
    
    _order = 'requested_date, id'
    
    # Escribimos el nuevo campo 'requested_date' del albarán de salida a partir del del PV.
    def action_ship_create(self, cr, uid, ids, context=None):
        res = super(sale_order, self).action_ship_create(cr, uid, ids, context=context)
        for order in self.browse(cr, uid, ids):
            for picking in order.picking_ids:
                if picking.type == 'out':
                    self.pool.get('stock.picking').write(cr, uid, [picking.id], {'requested_date': order.requested_date })
        return res

sale_order()