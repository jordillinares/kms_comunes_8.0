# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    'name' : 'KM Sistemas - Sale simple requested date',
    'version' : '1.0',
    'author' : 'KM Sistemas de información, S.L.',
    'category' : '',
    'description' : """
    This module adds a 'requested_date' field to the sales order. Tree view elements are
    now ordered by requested date + creation date (older first).
    
    Please note that this module is useful ONLY if any of your customers specify a precise
    date for receiving the goods. If you need to manage manufacturing or procurement times,
    or a MAXIMUM delivery date, you'd better use sale_order_dates module or some other
    community modules. This module is much simpler than sale_order_dates, because it's 
    not intended to calculate periods between requested and planned dates, and some other
    features that sale_order_dates implements are also not needed.
    """,
    'website': 'http://www.kmsistemas.com',
    'images' : [
        '',
    ],
    'depends' : [
        'sale',
    ],
    'conflicts' : [
        'sale_order_dates',
    ],
    'data': [
        'sale_order_view.xml',
        'stock_view.xml',
    ],
    'qweb' : [
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}