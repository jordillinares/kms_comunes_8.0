# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv, fields


class res_partner(osv.Model):

    _inherit = 'res.partner'

    def onchange_country_id(self, cr, uid, ids, country_id):
        fiscal_positions = None
        if country_id:
            country_obj = self.pool.get('res.country')
            fiscal_pos_obj = self.pool.get('account.fiscal.position')
            country = country_obj.browse(cr, uid, country_id)
            # Por ahora, ZZ quiere que todos los clientes de Portugal, Francia
            # y Bélgica, tanto particulares como empresas, cuenten como régimen nacional.
            #if country.code == 'ES':
            if country.code in ('ES','PT','FR', 'BE'):
                fiscal_positions = fiscal_pos_obj.search(cr, uid, [('name','=','Régimen Nacional')])
            else:
                intracom_countries = ['DE','AT','BE','BG','CY','DK','SI','EE','FI','FR','EL','GB','NL','HU','IT','IE','LV','LT','LU','MT','PL','PT','CZ','SK','RO']
                if country.code in intracom_countries:
                    fiscal_positions = fiscal_pos_obj.search(cr, uid, [('name','=','Régimen Intracomunitario')])
                else:
                    fiscal_positions = fiscal_pos_obj.search(cr, uid, [('name','=','Régimen Extracomunitario')])
        fiscal_position_id = fiscal_positions and fiscal_positions[0] or False
        return {'value': {'property_account_position': fiscal_position_id}}

res_partner()