Better User
===========

In a standard Odoo installation the user model has a last connection column.
This column is updated every time a user connects...

This is a dumb design decision, since PostgreSQL connections are configured to
rollback if anything related to the query is modified.

Effectively this provokes massive transactions rollbacks on systems with
more than a few hundred 'simultaneous' users.

This module corrects this problem.
