# -*- encoding: UTF-8 -*-

__author__ = 'faide'
from openerp.osv import fields
from openerp.osv import osv
import datetime
from openerp.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT


def default_dt(*args, **kwargs):
    return datetime.datetime.strftime(
        datetime.datetime.utcnow(), DEFAULT_SERVER_DATETIME_FORMAT
    )


class LoginLogs(osv.Model):
    _name = "login_audit"
    _description = "Login audit trail"
    _rec_name = "login"

    _order = "login_dt desc"

    _columns = {
        "login": fields.char(
            "login", size=64, required=True,
            help="The login name that performed a login operation"
        ),
        "login_dt": fields.datetime(
            "login_dt", help="Datetime of login operation"
        ),
    }

    _defaults = {
        "login_dt": default_dt
    }
