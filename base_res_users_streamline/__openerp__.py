# -*- coding: utf-8 -*-
##############################################################################
# Better User login for Odoo
# Author: Florent AIDE, <florent.aide@xcg-consulting.fr>
# Date: 11/2014
##############################################################################
{
    "name": "base_res_users_streamline",
    "version": "1.0",
    "author": "XCG Consulting",
    "category": '',
    "description": """
    An addon you'll need to install if you plan to have a heavy number of users
    in your system.

    This will fix the following error:

     TransactionRollbackError: could not serialize access due to concurrent
     update
     CONTEXT:  SQL statement "SELECT 1 FROM ONLY "public"."res_users" x WHERE
     "id" OPERATOR(pg_catalog.=) $1 FOR KEY SHARE OF x"


    ATTENTION though: this module will render the "last login" column on your
    users definition useless since it disables the SQL updates associated to
    the user login phase to obtain better performances.
    """,
    'website': 'http://odoo.consulting',
    'init_xml': [],
    "depends": [
        'base',
    ],
    "data": [
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'active': False,
    'css': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
