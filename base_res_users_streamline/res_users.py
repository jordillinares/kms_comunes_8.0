# -*- coding: utf-8 -*-
import openerp
import logging

from openerp.osv import osv
from openerp import SUPERUSER_ID
from openerp import pooler

_logger = logging.getLogger(__name__)


class BetterUser(osv.Model):
    """Here we inherit the res.users model to override the login phase and
    remove the write operation that logs the login time of every user in the
    system in res_users table... This is a GROSS error since it will
    systematically rollback ALL ongoing transactions that are somehow linked
    to the user.

    Here is a simple exemple: John is an accountant and is the person who
    creates most of the invoices in your accounting books. But Jean is the
    financial controller and she is responsible to audit the books and
    periodically has to print trial balances and other such reports. If John
    logs in during Jean's SQL request that is constructing the trial
    balance... Guess what? Jean's request will be rolled back :)

    But on a lowly charged system you'll not notice this fact because the
    ORM will auto resubmit the SQL request for you a series of times before it
    considers this a failure and actually returns an error.
    Now the real scenario is a system with hundreds of people who login/logout
    throughout the day.

    In this scenario you'll get a login every second or more ;p now guess what?
    Your system will use much of its time updating the user table and throwing
    rollbacks to the people already in the system :)

    Lets fix this mess.
    """
    _inherit = 'res.users'

    def _login(self, db, login, password):
        if not password:
            # if no password is given we just refuse the login
            return False

        user_id = False
        cr = pooler.get_db(db).cursor()

        try:
            res = self.search(cr, SUPERUSER_ID, [('login', '=', login)])

            if res:
                user_id = res[0]

                # this call could raise AccessDenied...
                self.check_credentials(cr, user_id, password)

                login_audit_osv = self.pool.get('login_audit')
                values = {"login": user_id}
                # log the login in another table
                login_audit_osv.create(cr, SUPERUSER_ID, values)
                cr.commit()

        except openerp.exceptions.AccessDenied:
            _logger.info("Login failed for db:%s login:%s", db, login)
            user_id = False

        finally:
            # explicitly close the cursor in ANY case
            cr.close()

        return user_id
