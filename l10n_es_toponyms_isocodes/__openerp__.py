# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    'name' : 'KM Sistemas - ISO Codes on spanish provinces.',
    'version' : '1.0',
    'author' : 'KM Sistemas de información, S.L.',
    'category' : 'Localisation/Europe',
    'description' : """
    This module adds an 'iso_code' column to res_country_state, which represents the ISO-3166-2
    for every province/state of a country.
    This allows to exchange information with geospatial BI apps that use the same standard, such as Saiku.
    It also sets foretold ISO code for spanish provinces.
    """,
    'website': 'http://www.kmsistemas.com',
    'images' : [
        '',
    ],
    'depends' : [
        'l10n_es_toponyms',
    ],
    'data': [
        'l10n_es_toponyms_states_both.xml',
        'l10n_es_toponyms_states_official.xml',
        'l10n_es_toponyms_states_spanish.xml'
    ],
    'qweb' : [
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}