UPDATE account_journal SET group_invoice_lines=True, sequence_id=sq_accmoves.id, invoice_sequence_id=sq_invoices.id, update_posted=True 
FROM ir_model_data md_accmoves, ir_model_data md_invoices, ir_sequence sq_accmoves, ir_sequence sq_invoices
WHERE account_journal.code='COMPR' 
AND md_accmoves.model='ir.sequence' AND md_accmoves.module='l10n_es' AND md_accmoves.name='sequence_journal' AND sq_accmoves.id=md_accmoves.res_id
AND md_invoices.model='ir.sequence' AND md_invoices.module='kms_base'  AND md_invoices.name='seq_in_invoice' AND sq_invoices.id=md_invoices.res_id;

UPDATE account_journal SET group_invoice_lines=True, sequence_id=sq_accmoves.id, invoice_sequence_id=sq_invoices.id, update_posted=True 
FROM ir_model_data md_accmoves, ir_model_data md_invoices, ir_sequence sq_accmoves, ir_sequence sq_invoices
WHERE account_journal.code='ACOMP'
AND md_accmoves.model='ir.sequence' AND md_accmoves.module='l10n_es' AND md_accmoves.name='sequence_journal' AND sq_accmoves.id=md_accmoves.res_id
AND md_invoices.model='ir.sequence' AND md_invoices.module='kms_base'  AND md_invoices.name='seq_in_refund' AND sq_invoices.id=md_invoices.res_id;

UPDATE account_journal SET group_invoice_lines=True, sequence_id=sq_accmoves.id, invoice_sequence_id=sq_invoices.id, update_posted=True, allow_date=True 
FROM ir_model_data md_accmoves, ir_model_data md_invoices, ir_sequence sq_accmoves, ir_sequence sq_invoices
WHERE account_journal.code='VEN'
AND md_accmoves.model='ir.sequence' AND md_accmoves.module='l10n_es' AND md_accmoves.name='sequence_journal' AND sq_accmoves.id=md_accmoves.res_id
AND md_invoices.model='ir.sequence' AND md_invoices.module='kms_base'  AND md_invoices.name='seq_out_invoice' AND sq_invoices.id=md_invoices.res_id;

UPDATE account_journal SET group_invoice_lines=True, sequence_id=sq_accmoves.id, invoice_sequence_id=sq_invoices.id, update_posted=True, allow_date=True 
FROM ir_model_data md_accmoves, ir_model_data md_invoices, ir_sequence sq_accmoves, ir_sequence sq_invoices
WHERE account_journal.code='AVENT'
AND md_accmoves.model='ir.sequence' AND md_accmoves.module='l10n_es' AND md_accmoves.name='sequence_journal' AND sq_accmoves.id=md_accmoves.res_id
AND md_invoices.model='ir.sequence' AND md_invoices.module='kms_base'  AND md_invoices.name='seq_out_refund' AND sq_invoices.id=md_invoices.res_id;

UPDATE account_journal SET sequence_id=sq_accmoves.id FROM ir_model_data md_accmoves, ir_sequence sq_accmoves
WHERE account_journal.code='OPEJ' AND md_accmoves.model='ir.sequence' AND md_accmoves.module='l10n_es' AND md_accmoves.name='sequence_journal'
AND sq_accmoves.id=md_accmoves.res_id;