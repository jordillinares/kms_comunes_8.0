# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv, fields




class ir_sequence(osv.Model):

    _inherit = 'ir.sequence'

    def _next(self, cr, uid, seq_ids, context = None):
        # Aquí sobrecargamos el _next del módulo account, para tratar las secuencias que no pertenecen a facturas
        # pero sí tienen varios ejercicios fiscales asociados, junto con sus respectivas secuencias secundarias (una por ej.)
        if context is None:
            context = {}
        for seq in self.browse(cr, uid, seq_ids, context):
            for line in seq.fiscal_ids:
                # Esto sólo vale para fras. ya que sólo en la contabilidad se usa 'fiscalyear_id' en contexto
                if line.fiscalyear_id.id == context.get('fiscalyear_id'):
                    return super(ir_sequence, self)._next(cr, uid, [line.sequence_id.id], context)
                # Para todo lo demás (pedidos, albaranes...), comparamos el fiscalyear de la secuencia con el fiscalyear de la fecha actual
                elif line.fiscalyear_id.id == self.pool.get('account.fiscalyear').find(cr, uid, exception = False):
                    return super(ir_sequence, self)._next(cr, uid, [line.sequence_id.id], context)
        # Y ya si no lo encotramos, que se comporte como el _next del original de ir.sequence
        return super(ir_sequence, self)._next(cr, uid, seq_ids, context)

ir_sequence()