# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv, fields
from openerp.tools.translate import _


class res_partner_bank(osv.Model):

    _inherit = 'res.partner.bank'
    
    def post_write(self, cr, uid, ids, context=None):
        if isinstance(ids, (int, long)):
            ids = [ids]
        obj_acc = self.pool.get('account.account')
        for bank in self.browse(cr, uid, ids, context):
            if bank.company_id and not bank.journal_id:
                # Find the code and parent of the bank account to create
                #dig = 6
                current_num = 1
                ids = obj_acc.search(cr, uid, [('type','=','liquidity'), ('company_id', '=', bank.company_id.id), ('parent_id', '!=', False)], context=context)
                # No liquidity account exists, no template available
                if not ids: continue
                dig = max([len(x.code) for x in obj_acc.browse(cr, uid, ids)])
                ref_acc_bank = obj_acc.browse(cr, uid, ids[0], context=context).parent_id
                while True:
                    new_code = str(ref_acc_bank.code.ljust(dig-len(str(current_num)), '0')) + str(current_num)
                    ids = obj_acc.search(cr, uid, [('code', '=', new_code), ('company_id', '=', bank.company_id.id)])
                    if not ids:
                        break
                    current_num += 1
                name = self._prepare_name(bank)
                acc = {
                    'name': name,
                    'code': new_code,
                    'type': 'liquidity',
                    'user_type': ref_acc_bank.user_type.id,
                    'reconcile': False,
                    'parent_id': ref_acc_bank.id,
                    'company_id': bank.company_id.id,
                }
                acc_bank_id  = obj_acc.create(cr,uid,acc,context=context)
                jour_obj = self.pool.get('account.journal')
                new_code = 1
                while True:
                    code = _('BNK')+str(new_code)
                    ids = jour_obj.search(cr, uid, [('code','=',code)], context=context)
                    if not ids:
                        break
                    new_code += 1
                #create the bank journal
                vals_journal = {
                    'name': name,
                    'code': code,
                    'type': 'bank',
                    'company_id': bank.company_id.id,
                    'analytic_journal_id': False,
                    'default_credit_account_id': acc_bank_id,
                    'default_debit_account_id': acc_bank_id,
                }
                journal_id = jour_obj.create(cr, uid, vals_journal, context=context)
                self.write(cr, uid, [bank.id], {'journal_id': journal_id}, context=context)
        return True

res_partner_bank()