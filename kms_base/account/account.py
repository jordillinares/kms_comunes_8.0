# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp import models, fields, api



class account_tax(models.Model):

    _inherit = 'account.tax'
    
    def _unit_compute(self, cr, uid, taxes, price_unit, product=None, partner=None, quantity=0):
        res = super(account_tax, self)._unit_compute(cr, uid, taxes, price_unit, product=None, partner=None, quantity=0)
        for data in res:
            for tax in taxes:
                if data['id'] == tax.id:
                    data['name'] = tax.description or tax.name # Pero no las doooos!
        return res 
        
account_tax()


class account_invoice_line(models.Model):
    
    _inherit = 'account.invoice.line'
    
    # Overload product_id_change to avoid including product's description_sale in IL name
    @api.multi
    def product_id_change(self, product, uom_id, qty=0, name='', type='out_invoice',
            partner_id=False, fposition_id=False, price_unit=False, currency_id=False,
            company_id=None):
        res = super(account_invoice_line, self).product_id_change(product, uom_id, qty, name, type,
            partner_id, fposition_id, price_unit, currency_id, company_id)
        if product:
            part = self.env['res.partner'].browse(partner_id)
            if part.lang:
                self = self.with_context(lang=part.lang)
            product = self.env['product.product'].browse(product)
            res['value']['name'] = product.partner_ref
        return res
    
    