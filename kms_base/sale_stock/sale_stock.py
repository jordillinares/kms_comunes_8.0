# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2015 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import osv



class sale_order_line(osv.Model):

    _inherit = 'sale.order.line'
    
    # En lugar de dar una advertencia sobre que la cantidad introducida no es múltiplo del
    # empaquetado y sugerir que se propone una cantidad que sí lo sea, cosa que no hace en
    # realidad, se quita el warning y se fuerza que la cantidad sea el siguiente múltiplo
    # superior.
    def product_packaging_change(self, cr, uid, ids, pricelist, product, qty=0, uom=False,
                                   partner_id=False, packaging=False, flag=False, context=None):
        product_obj = self.pool.get('product.product')
        product_uom_obj = self.pool.get('product.uom')
        pack_obj = self.pool.get('product.packaging')
        products = product_obj.browse(cr, uid, product, context=context)
        if packaging:
            default_uom = products.uom_id and products.uom_id.id
            pack = pack_obj.browse(cr, uid, packaging, context=context)
            q = product_uom_obj._compute_qty(cr, uid, uom, pack.qty, default_uom)
            if qty and (q and not (qty % q) == 0):
                qty = (int(qty / q) + 1 ) * q
        return super(sale_order_line, self).product_packaging_change(cr, uid, ids, pricelist,
                                                                    product, qty, uom, partner_id,
                                                                    packaging, flag, context)

sale_order_line()