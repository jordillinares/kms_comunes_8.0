# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution    
#    Copyright (C) 2004-2012 Tiny SPRL (http://tiny.be). All Rights Reserved   
#
#    This module,
#    Copyright (C) 2014 KM Sistemas de Información, S.L. - http://www.kmsistemas.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    'name' : 'Módulo común a todas las implantaciones de Odoo 8.0 por KM Sistemas',
    'version' : '1.0',
    'author' : 'KM Sistemas de información, S.L.',
    'category' : '',
    'description' : """
Este módulo incorpora funcionalidades o cambios comunes a todos los proyectos de Odoo en las siguientes áreas:

Base:
-    Secuencias: analiza las secuencias distintas a las de factura y les asocia el contador correspondiente al año
     fiscal actual, asumiendo que se han predefinido una secuencia plantilla y sus n secuencias hijas para n
     ejercicios fiscales.

-    Grupos de usuarios: ahora se puede duplicar un grupo de usuarios, ya que se redefine el campo category_id con
     copy=False.

Contabilidad:
-    Crea ejercicios fiscales desde 2013 hasta 2020, y para cada uno le crea periodos trimestrales.
-    Crea secuencias anuales para asientos, pedidos, albaranes y facturas, vinculándolas a cada ejercicio fiscal.
-    Enlaza los diarios de compra y venta con las correspondientes secuencias de fra, y les configura algunos checkboxes.
-    Asigna grouping '[3,0]' para todos los idiomas instalados, para agrupar y formatear los millares.
-    Sobrecarga el campo 'name' de account.payment del módulo account_payment para que sea Translatable.

Mail:
-    Elimina de algunos correos (los enviados a los followers del registro) el mensaje de "Enviado por %(company)s
     usando %(odoo)s".

Ventas:
-    Evita que la LPV incluya en su campo 'name' el campo 'description_sale' del producto.
-    Se ha redefinido el product_packaging_change de sale_stock para que, en los productos para los que se define empaquetado,
     en lugar de dar una advertencia sobre que la cantidad introducida no es múltiplo del empaquetado y sugerir que se propone
     una cantidad que sí lo sea, cosa que no hace en realidad, se quita el warning y se fuerza que la cantidad sea el múltiplo
     superior inmediato.
     
Facturación:
-    Evita que la LF incluya en su campo 'name' el campo 'description_sale' del producto.

    """,
    'website': 'http://www.kmsistemas.com',
    'images' : [
    ],
    'depends' : [
        'account',
        'account_payment',
        'l10n_es',
        'l10n_es_account_invoice_sequence',
        'sale',
        'mail',
        'sale_stock',
    ],
    'conflicts' : [
    ],
    'data': [
        'fiscalyear_data.xml',
        'sequences_per_fiscalyear_data.xml',
        'update_journal_data.sql',
        'update_thousand_separator.sql',
    ],
    'qweb' : [
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}